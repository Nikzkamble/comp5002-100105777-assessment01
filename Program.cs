﻿using System;

namespace comp5002_100105777_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            string name, option;
            double num, gst;


            Console.Clear();
            Console.WriteLine("Welcome to DreamTech");//Display "Welcome to DreamTech shop"//

            Console.WriteLine("Enter Your name:");//Ask user to write their name//
            name = Console.ReadLine();
            Console.WriteLine($"Hi {name}, Welcome to the DreamTech");//Display "Hi(name),Welcome to DreamTech //
            Console.WriteLine("Write price of product with Decimal places");//Ask user to add price of product in decimals//
            num = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Would  you like to add another product?");//Ask if user want's to add another number
            Console.WriteLine("Type y for yes or n for no");//Ask user to type y for yes or n for NO//
            option = Console.ReadLine();
            if (option == "y")
            {
                Console.WriteLine("Enter price of another product");//Enter the price of 2nd product//
                num += Convert.ToDouble(Console.ReadLine());
            }
            gst = 0.15;
            Console.WriteLine(string.Format("Net total = ${0:0.##}", num));//Display user the Net total//
            Console.WriteLine(string.Format("Gst=${0:0.##}", num * gst));//Display the gst which will add to Net total//
            Console.WriteLine(string.Format("Gross Total=${0:0.##}", num + (num * gst)));//Add Net total and gst then Display Gross total//
            Console.WriteLine("Thank You For Shopping with us,Please come again");//Display"Thank you for shopping with us,please come again"
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();


        }
    }
}